﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private float forceShot;
    [SerializeField]
    private Transform spawnPosition;
    [SerializeField]
    private Transform weapon;

    private int countAmmo;

    private void Start()
    {
        countAmmo = 10;
    }

    public void AddAmmo(int countAmmo)
    {
        this.countAmmo += countAmmo;
    }

    private void Update()
    {
        Debug.DrawRay(spawnPosition.position, (spawnPosition.position - weapon.position).normalized * 20, Color.blue);
        if (Input.GetMouseButtonDown(0) && countAmmo > 0)
        {
            Ray ray = new Ray(spawnPosition.position, (spawnPosition.position - weapon.position).normalized);
            RaycastHit raycast;
            if (Physics.Raycast(ray, out raycast, 1000))
            {
                if (raycast.transform.CompareTag("Enemy"))
                {
                    raycast.transform.GetComponent<Enemy>().Damage(20);
                }
                GameObject obj = Instantiate(bulletPrefab, raycast.point, Quaternion.identity);
                obj.transform.parent = raycast.transform;
            }
            countAmmo--;
        }
    }
}
