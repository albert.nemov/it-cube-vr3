﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private float sensitivity;
    [SerializeField]
    private Transform playerTransform;

    private float angleX;

    private void Start()
    {
        angleX = 0;
    }

    private void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        angleX -= mouseY * sensitivity;

        angleX = Mathf.Clamp(angleX, -50, 50);

        playerTransform.Rotate(new Vector3(0, mouseX, 0) * sensitivity);
        transform.localRotation = Quaternion.Euler(angleX, 0, 0);
    }
}
